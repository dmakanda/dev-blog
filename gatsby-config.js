module.exports = {
  siteMetadata: {
    title: 'DEV BLOG',
    description: 'blog with resources and resume'
  },
  plugins: [
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'files',
        path: `${__dirname}/src/markdown`
      },
    },
    'gatsby-transformer-remark'
  ]
}
