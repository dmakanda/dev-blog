import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Article from "../components/article"

export default function ArticleList() {
    return (
        <StaticQuery
            query={graphql`query {
  
                allMarkdownRemark(sort: {fields: [frontmatter___date], order: DESC}) {
                  edges {
                    node {
                      id
                      fields {
                          slug
                    }
                      frontmatter {
                                  title
                        image
                        keywords
                        date(formatString: "DD/MM/YYYY")
                      }
                      excerpt
                    }
                  }
                  totalCount
                }
              
              
              }`
            }

            render={data => (
                <div>
                    {data.allMarkdownRemark.edges.map(({ node }) => (
                        <Article
                            to={node.fields.slug}
                            id={node.id}
                            keywords={node.frontmatter.keywords}
                            title={node.frontmatter.title}
                            date={node.frontmatter.date}
                            excerpt={node.excerpt}
                        />
                    ))}
                </div>
            )

            }
        />
    )
}
