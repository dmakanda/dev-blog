import React from "react"
import * as styles from "./footer.module.scss"

export default function Footer({children}) {
    return (
        <footer className={styles.container}>
            <div className={styles.footer}>
                {children}
            </div>
        </footer>
    )
}