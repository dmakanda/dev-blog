import React from "react"
import { Link, StaticQuery, graphql } from "gatsby"
import * as styles from "./header.module.scss"

const HeaderLink = props => (
    <Link className={styles.link} to={props.to}>{props.text}</Link>
)

const HomeButton = props => (
    <Link className={styles.button} to={props.to}>{props.text}</Link>
)

const SocialButton = props => {
    let style = ''
    let url = ''

    if (props.site === 'linkedin') {
        style = styles.buttonLinkedin
        url = `https://www.linkedin.com/in/${props.username}`
    }

    if (props.site === 'github') {
        style = styles.buttonGithub
        url = `https://www.github.com/${props.username}`
    }

    return (
        <a href={url} target="_blank" rel="noopener noreferrer">
            <div className={style}>{props.children}&nbsp;</div>
        </a>
    )
}

export default function Header() {
    return (

        <StaticQuery
            query={graphql`
                query {
                    site {
                        siteMetadata {
                            title
                        }
                    }
                }`
            }
            render={data => (
                <header className={styles.container}>
                    <div className={styles.row}>
                        <HomeButton to="/" text={data.site.siteMetadata.title} />
                        <SocialButton site="github" username="delitamakanda" />
                        <SocialButton site="linkedin" username="delitamakanda" />
                    </div>
                    <nav className={styles.row}>
                        <HeaderLink to="/" text="BLOG" />
                        <HeaderLink to="/about" text="CV" />
                    </nav>
                </header>)
            }

        />
    )
}