import React from "react";
import Footer from "./footer";
import Header from "./header";
import * as styles from "./layout.module.scss";

export default function Layout({ children }) {
    return (
        <div className={styles.container}>
            <Header />
            <div className={styles.content}>
                {children}
            </div>
            <Footer>Made with Gatsby</Footer>
        </div>
    )
}