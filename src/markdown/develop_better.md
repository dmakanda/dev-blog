---
title: "Develop better for yourself"
date: "2021-03-10"
image: "https://source.unsplash.com/150x150/?developer"
keywords: "developer"
---

# Resources

1. https://stackoverflow.blog/2021/03/03/best-practices-can-slow-your-application-down/?utm_source=Iterable&utm_medium=email&utm_campaign=the_overflow_newsletter
2. https://javascript.plainenglish.io/creating-a-chrome-extension-with-react-d92db20550cb
3. https://www.elmghari.com/talent-stack/
4. https://kittygiraudel.com/2021/02/17/hiding-content-responsibly/



