import React from "react"
import ArticleList from "../components/article-list"
import Layout from "../components/layout"
import Title from "../components/title"

export default function Home() {
  return (
    <Layout>
      <Title text="Bienvenu"></Title>
      <ArticleList />
    </Layout>
  )
}
